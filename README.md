# NWB Tutorials

## Requirements

You should have these installed already:

- `git` 
- `conda` via `miniconda` or `anaconda`
- `HDFView` (optional)

    <details>
        <summary>
            Click to show sources of <code>HDFView</code>
        </summary>
        
    You can install up-to-date `HDFView` with these sources:
    - https://flathub.org/apps/details/org.hdfgroup.HDFView
    - https://formulae.brew.sh/cask/hdfview
    - https://portal.hdfgroup.org/display/HDFVIEW/HDFView
    - https://search.nixos.org/packages#?channel=22.11&show=hdfview&from=0&size=50&sort=relevance&type=packages&query=hdfview
    
    </details>

To start, clone the repository and create the environment:

```bash 
git clone https://gitlab.com/fleischmann-lab/workshops/nwb-tutorial
cd nwb-tutorial
conda env create -f environment.yml
```

Then activate with:

```bash
conda activate nwb-tutorial
```

<details>
    <summary>
        [Optional] Click to show how to install <code>HDFView</code> in <code>conda</code>
    </summary>
    
Please note that this `HDFView` vesion is **NOT** up-to-date, but if you want to install in this `conda` environment, do this:
    
```bash
conda activate nwb-tutorial # if you haven't done so
conda install -c eumetsat hdfview
```
    
To start, type `hdfview` in your terminal.
    
If you prefer to use more updated version, see the above notes
    
</details>

Then do this to open notebooks:

```bash
jupyter lab
```