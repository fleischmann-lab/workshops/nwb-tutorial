# Spontaneous odor PCx experiment

Courtesy of Max Seppo.

This is one of Max's spontaneous & passive odor presentation experiment in the piriform cortex, with additional *experimental* facemap top 10 components. 

This was converted with `calimag` with some [modifications](https://gitlab.com/fleischmann-lab/calcium-imaging/pcx-spont-odor/-/blob/main/notebooks/batch-calimag.ipynb). 

Note: many fields are *experimental*-ly integrated, so use with caution. Also, the backgrounds are [duplicated](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/112).

