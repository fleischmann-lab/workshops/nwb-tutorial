# `calimag` test data

These two are the data from `calimag` test suite (those that have config files). Note: these were conveted with `calimag` from this branch [`issue/fix-plane-idx-and-background`](https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/tree/issue/fix-plane-idx-and-background) to fix the duplicated backgrounds and consistent plane indexing names. 

