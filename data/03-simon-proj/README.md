# PCx projection experiments

Courtesy of Simon Daste.

This is one of Simon's passive odor presentation with labeled projections experiment in the piriform cortex, with additional *experimental* additions (odor table, lab metadata extension and post-hoc red labeled cell list).

The file names and folder structures must stay the same to read.

```
├── preproc
│   └── OB_mice
│       └── Mouse#8_passive.nwb
└── proc
    └── mouse_008_passive.nwb
```

