# DandiSet `000037` *Allen Institute Openscope - Credit Assignment project*

This is one of the data from the Allen Brain Institute, obtained from [`dandiarchive`](https://dandiarchive.org/dandiset/000037/).

You can download it from visiting this [link](https://dandiarchive.org/dandiset/000037/draft/files?location=sub-411424), and click on the download icon for the files you want to download.

If you want to download the whole dataset, you can use the `dandi` CLI (remember to first `pip install dandi`), for example like this:

```bash
![ -d allen-data ] || mkdir allen-data
!dandi download --existing skip \
    --output-dir allen-data \
	"https://dandiarchive.org/dandiset/000037"
```

For more information on `dandi` CLI or their `python` API, please visit their [documentation](https://dandi.readthedocs.io/en/latest/cmdline/download.html).

